Small bot which posts every sunday evening a reminder to a discord channel, so that people don't forget their chores in the browser game _Ogame_.

## Deploy
1. do code changes
2. build & push docker images with `heroku container:push -a avocado-bot worker`
3. use docker images for pods with `heroku container:release -a avocado-bot worker`
4. restart all dynos in heroku webinterface
