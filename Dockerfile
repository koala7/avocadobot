FROM python:3.7-slim

ENV PYTHONUNBUFFERED 1

RUN pip install schedule
RUN pip install requests

COPY avocado-bot.py /opt/

CMD ["python", "/opt/avocado-bot.py"]

