#!/usr/bin/env python3

import time
import os
import sys

import schedule
import requests

WEBHOOK_URL = os.getenv('AVOCADO_WEBHOOK_URL')

if not WEBHOOK_URL:
    print("No webhook url supplied. Shutting down..")
    sys.exit()
print("found webhook. Continue program")

def remindOfTfRenewal():
    print("Renew your freaking TFs. They will be deleted at night!")
    payload = {
            "content": "**TF-REMINDER:** Alle bitte heute Nacht zwischen 01:00 und 03:00 Uhr eure TFs anfliegen damit sie nicht gelöscht werden! @everyone",
            "username": "Avocado-Bot"
    }
    r = requests.post(WEBHOOK_URL, json=payload)
    print(r.text)

schedule.every().sunday.at("18:00").do(remindOfTfRenewal)

while True:
    schedule.run_pending()
    time.sleep(60)
